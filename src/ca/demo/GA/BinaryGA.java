package ca.demo.GA;

import ca.demo.mlp.MLP;
import ca.demo.main.Record;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

/**
 * Created by CANCHU on 2016/11/7.
 */
public class BinaryGA {
    private static final int LEAVECOUNT = 30;
    private static final int ITERATECOUNT = 30000;

    private int HIDDEN_LAYER_COUNT = 1;
    private int HIDDEN_LAYER_NODE_COUNT = 2;

    private ArrayList<Record> list;
    private Chromosome[] chromosomes;
//    CSVWriter csvWriter;

    public BinaryGA(ArrayList<Record> list) {
        this.list = list;
        chromosomes = new Chromosome[LEAVECOUNT];
        for (int i = 0; i < LEAVECOUNT; i++) {
            chromosomes[i] = new Chromosome(list.get(0).getAttrLength(), HIDDEN_LAYER_COUNT, HIDDEN_LAYER_NODE_COUNT, false);
        }
    }

    public BinaryGA(ArrayList<Record> list, int HIDDEN_LAYER_COUNT, int HIDDEN_LAYER_NODE_COUNT, boolean useFeatureSelection) {
        this.HIDDEN_LAYER_COUNT = HIDDEN_LAYER_COUNT;
        this.HIDDEN_LAYER_NODE_COUNT = HIDDEN_LAYER_NODE_COUNT;
        this.list = list;
        chromosomes = new Chromosome[LEAVECOUNT];
        for (int i = 0; i < LEAVECOUNT; i++) {
            chromosomes[i] = new Chromosome(list.get(0).getAttrLength(), HIDDEN_LAYER_COUNT, HIDDEN_LAYER_NODE_COUNT, useFeatureSelection);
            try {
                chromosomes[i].setAccurancy(getfitness(chromosomes[i]));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public double run(String filename, double goodEnough) {

        double lastAccuracy = -1;
        int outputTimes = 100;

        try {
            File file = new File("MyOutput");
            CSVWriter csvWriter = new CSVWriter(new FileWriter(file.getAbsolutePath() + "\\" + filename + "_iteration.csv"), ',');
            StringBuilder stringBuilder = new StringBuilder("iteration&");
            if (chromosomes[0].getFEATHER_LENGTH() != 0) {
                stringBuilder.append("Selected Feature&");
            }
            stringBuilder.append("Accurancy&&");
            int weightCount = ((list.get(0).getAttrLength() + 1) * HIDDEN_LAYER_NODE_COUNT) + ((HIDDEN_LAYER_COUNT - 1) * ((HIDDEN_LAYER_NODE_COUNT + 1) * HIDDEN_LAYER_NODE_COUNT)) + (HIDDEN_LAYER_NODE_COUNT + 1);
            for (int i = 0; i < weightCount; i++) {
                stringBuilder.append("w").append(i).append("&");
            }
            String str = stringBuilder.toString();
            Write(csvWriter, str, "&");

            int sameTimes = 0;
            int iterateTimes = 0;
            for (; iterateTimes <= ITERATECOUNT && lastAccuracy < goodEnough && sameTimes < 5000; iterateTimes++) {
                //mutation
                Chromosome[] mutationChromosomes = new Chromosome[LEAVECOUNT];
                Random random = new Random();
                for (int i = 0; i < LEAVECOUNT; i++) {
                    double weightMutation_rate = (double) random.nextInt(10000) / 10000;
                    double featureMutation_rate = (double) random.nextInt(10000) / 10000;
                    mutationChromosomes[i] = chromosomes[i].getMutation(weightMutation_rate, featureMutation_rate);
                }
                //crossover
                Chromosome[] crossoverChromosomes = new Chromosome[LEAVECOUNT];
                for (int i = 0; i < LEAVECOUNT; i++) {
                    int first = random.nextInt(LEAVECOUNT);
                    int second;
                    do {
                        second = random.nextInt(LEAVECOUNT);
                    } while (first == second);

                    double cross_rate = (double) random.nextInt(10000) / 10000;
                    crossoverChromosomes[i] = chromosomes[first].getCrossover(chromosomes[second], cross_rate);
                }
                //selection
                for (Chromosome chromosome : mutationChromosomes) {
                    chromosome.setAccurancy(getfitness(chromosome));
                }
                for (Chromosome chromosome : crossoverChromosomes) {
                    chromosome.setAccurancy(getfitness(chromosome));
                }
                selection(chromosomes, mutationChromosomes, crossoverChromosomes);


                if (iterateTimes % outputTimes == 0) {
                    for (int j = 0; j < 10; j++) {
                        outputChromosome(chromosomes[j], iterateTimes, csvWriter);
                    }
                    str = "&";
                    Write(csvWriter, str, "&");
                }

                if (lastAccuracy != -1) {
                    if (lastAccuracy == chromosomes[0].getAccurancy()) {
                        sameTimes++;
                    } else {
                        sameTimes = 0;
                        lastAccuracy = chromosomes[0].getAccurancy();
                    }
                } else {
                    lastAccuracy = chromosomes[0].getAccurancy();
                }
            }
            if (iterateTimes % outputTimes != 0) {
                for (int j = 0; j < 10; j++) {
                    outputChromosome(chromosomes[j], iterateTimes, csvWriter);
                }
            }
            csvWriter.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return lastAccuracy;
    }

    private void outputChromosome(Chromosome chromosome, int times, CSVWriter csvWriter) {
        double[][][] weight = chromosome.getWeight2(chromosome.getSelectFeature());
        StringBuilder stringBuilder = new StringBuilder(String.valueOf(times)).append("&");

        Integer[] selectFeature = chromosome.getSelectFeature();
        if (selectFeature != null) {
            for (int featureIndex : selectFeature) {
                stringBuilder.append(featureIndex).append(" ");
            }
        }

        stringBuilder.append("&").append(chromosome.getAccurancy()).append("&");

        for (double[][] aWeight : weight) {
            for (double[] anAWeight : aWeight) {
                for (double anAnAWeight : anAWeight) {
                    stringBuilder.append("&").append(anAnAWeight);
                }
            }
        }

        Write(csvWriter, stringBuilder.toString(), "&");
    }

    private void selection(Chromosome[] A, Chromosome[] B, Chromosome[] C) {
        int s = 0;
        Chromosome[] All = new Chromosome[A.length + B.length + C.length];
        for (Chromosome Chromosome : A) {
            All[s++] = Chromosome;
        }
        for (Chromosome Chromosome : B) {
            All[s++] = Chromosome;
        }
        for (Chromosome Chromosome : C) {
            All[s++] = Chromosome;
        }

        //sort
        for (int i = 0; i < s - 1; i++) {
            for (int j = i + 1; j < s; j++) {
                if (All[i].getAccurancy() < All[j].getAccurancy()) {
                    Chromosome temp = All[i];
                    All[i] = All[j];
                    All[j] = temp;
                }
            }
        }

        //choose leave
        leaveMethod(All, 2);


    }

    private void leaveMethod(Chromosome[] All, int num) {
        // num=1 -> leave top10. ; num=2 -> leave top8 and random leave another randomNum.
        switch (num) {
            case 1:
                //top10
                chromosomes = null;
                chromosomes = Arrays.copyOfRange(All, 0, 10);
                break;
            case 2:
                int randomNum = 10;
                boolean[] ck = new boolean[All.length];
                chromosomes = new Chromosome[LEAVECOUNT];
                for (int i = 0; i < LEAVECOUNT - randomNum; i++) {
                    chromosomes[i] = new Chromosome(All[i]);
                    ck[i] = true;
                }

                for (int i = LEAVECOUNT - randomNum; i < LEAVECOUNT; i++) {
                    Random random = new Random();
                    int tmp;
                    do {
                        tmp = random.nextInt(All.length - (LEAVECOUNT - randomNum)) + (LEAVECOUNT - randomNum);
                    } while (ck[tmp]);
                    ck[tmp] = true;
                    chromosomes[i] = new Chromosome(All[tmp]);
                }
                break;
        }
    }

    private double getfitness(Chromosome chromosome) throws IOException {
        MLP MLP = new MLP(list.get(0).getAttrLength(), HIDDEN_LAYER_COUNT, HIDDEN_LAYER_NODE_COUNT);
        Integer[] selectedFeature = chromosome.getSelectFeature();
        MLP.initialParameter(chromosome.getWeight2(selectedFeature), selectedFeature);
        return MLP.classify(list, chromosome.getSelectFeature(), new double[]{0.5});
    }

    private void Write(CSVWriter writer, String str, String sym) {
        String[] entries = str.split(sym);
        writer.writeNext(entries);
    }
}
