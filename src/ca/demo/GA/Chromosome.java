package ca.demo.GA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Created by CANCHU on 2016/11/7.
 */
public class Chromosome {

    private final int WEIGHT_LENGTH = 9;

    private boolean[] chromosome;
    private int ChromosomeLength;

    private int HiddenLayerCount = 0;
    private int HiddenLayerNodesCount = 0;
    private int FEATHER_LENGTH = 0;
    private int attrNum;

    private double Accurancy;

    public Chromosome(int attrNum, int HiddenLayerCount, int HiddenLayerNodesCount, boolean featureSelection) {
        if (featureSelection) FEATHER_LENGTH = attrNum;
        this.attrNum = attrNum;
        this.HiddenLayerCount = HiddenLayerCount;
        this.HiddenLayerNodesCount = HiddenLayerNodesCount;
        //(Attr+1)*NC + (LN-1)((NC+1)*NC) + (NC+1)
        ChromosomeLength = (((attrNum + 1) * HiddenLayerNodesCount) + ((HiddenLayerCount - 1) * ((HiddenLayerNodesCount + 1) * HiddenLayerNodesCount)) + (HiddenLayerNodesCount + 1)) * WEIGHT_LENGTH + FEATHER_LENGTH;
        chromosome = new boolean[ChromosomeLength];
        Random random = new Random();
        setRadomChromosome(random.nextInt(ChromosomeLength));
    }

    public Chromosome(Chromosome c) {
        chromosome = c.getChromosome();
        ChromosomeLength = chromosome.length;
        attrNum = c.getAttrNum();
        FEATHER_LENGTH = c.getFEATHER_LENGTH();
        HiddenLayerNodesCount = c.getHiddenLayerNodesCount();
        HiddenLayerCount = c.getHiddenLayerCount();
        Accurancy = c.getAccurancy();
    }

    private Chromosome(Chromosome c1, Chromosome c2, double crossRate) {
        //use to get crossover
        boolean[] c1Bool = c1.getChromosome();
        boolean[] c2Bool = c2.getChromosome();
        ChromosomeLength = c1Bool.length;
        chromosome = new boolean[ChromosomeLength];
        attrNum = c1.getAttrNum();
        FEATHER_LENGTH = c1.getFEATHER_LENGTH();
        HiddenLayerNodesCount = c1.getHiddenLayerNodesCount();
        HiddenLayerCount = c1.getHiddenLayerCount();
        Random random = new Random();

        int crossLength = (int) (ChromosomeLength * crossRate); //使用crossRate計算從c1來的基因數量

        for (int i = 0; i < crossLength; i++) {
            int idx;
            do {
                idx = random.nextInt(ChromosomeLength);
            } while (chromosome[idx]); //找到未被挑過的index

            chromosome[idx] = true;
        }
        for (int i = 0; i < ChromosomeLength; i++) {
            chromosome[i] = chromosome[i] ? c1Bool[i] : c2Bool[i]; //若該位置為true，使用c1的基因，否則使用c2的基因
        }
    }

    private int getHiddenLayerCount() {
        return HiddenLayerCount;
    }

    private int getHiddenLayerNodesCount() {
        return HiddenLayerNodesCount;
    }

    public int getFEATHER_LENGTH() {
        return FEATHER_LENGTH;
    }

    private int getAttrNum() {
        return attrNum;
    }

    public void setChromosome(boolean[] Chromosome) {
        this.chromosome = Chromosome;
    }

    public void setAccurancy(double Accurancy) {
        this.Accurancy = Accurancy;
    }

    private void setRadomChromosome(int N) {
        while (N > 0) {
            Random random = new Random();
            int idx;
            do {
                idx = random.nextInt(ChromosomeLength);
            } while (chromosome[idx]);
            chromosome[idx] = true;
            N--;
        }
    }

    public boolean[] getChromosome() {
        return chromosome;
    }

    public double getAccurancy() {
        return Accurancy;
    }

    public double[][][] getWeight1() {
        //0~1
        double[][][] weight = new double[HiddenLayerCount + 1][][];
        for (int i = 0; i < HiddenLayerCount; i++) {
            weight[i] = new double[HiddenLayerNodesCount][];
            for (int j = 0; j < HiddenLayerNodesCount; j++) {
                if (i == 0) {
                    weight[i][j] = new double[attrNum + 1];
                } else {
                    weight[i][j] = new double[HiddenLayerNodesCount + 1];
                }
            }
        }
        weight[HiddenLayerCount] = new double[1][];
        weight[HiddenLayerCount][0] = new double[HiddenLayerNodesCount + 1];


        double[] oneDimensionalWeight = new double[((attrNum + 1) * HiddenLayerNodesCount) + ((HiddenLayerCount - 1) * ((HiddenLayerNodesCount + 1) * HiddenLayerNodesCount)) + (HiddenLayerNodesCount + 1)];
        int s = 0;
        for (int i = 0; i < chromosome.length - FEATHER_LENGTH; i += WEIGHT_LENGTH) {
            double v = 0;
            for (int j = 1; j < WEIGHT_LENGTH; j++) {
                if (chromosome[i + j])
                    v += Math.pow(2, j - 1);
            }
            if (chromosome[i]) {
                v *= -1;
            }
            oneDimensionalWeight[s++] = v;
        }
        s = 0;
        for (int j = 0; j < HiddenLayerNodesCount; j++) {
            double sum = 0;
            for (int k = 0; k <= attrNum; k++) {
                weight[0][j][k] = oneDimensionalWeight[s];
                sum += Math.abs(oneDimensionalWeight[s++]);
            }
            for (int k = 0; k <= attrNum; k++) {
                weight[0][j][k] /= sum;
            }
        }

        for (int i = 1; i < HiddenLayerCount; i++) {
            for (int j = 0; j < HiddenLayerNodesCount; j++) {
                double sum = 0;
                for (int k = 0; k <= HiddenLayerNodesCount; k++) {
                    weight[i][j][k] = oneDimensionalWeight[s];
                    sum += oneDimensionalWeight[s++];
                }
                for (int k = 0; k <= HiddenLayerNodesCount; k++) {
                    weight[i][j][k] /= sum;
                }
            }
        }

        double sum = 0;
        for (int k = 0; k <= HiddenLayerNodesCount; k++) {
            weight[HiddenLayerCount][0][k] = oneDimensionalWeight[s];
            sum += oneDimensionalWeight[s++];
        }
        for (int k = 0; k <= HiddenLayerNodesCount; k++) {
            weight[HiddenLayerCount][0][k] /= sum;
        }

        return weight;
    }

    public double[][][] getWeight2(Integer[] selectFeature) {
        //-2~2
        double[][][] weight = new double[HiddenLayerCount + 1][][];
        for (int i = 0; i < HiddenLayerCount; i++) {
            weight[i] = new double[HiddenLayerNodesCount][];
            for (int j = 0; j < HiddenLayerNodesCount; j++) {
                if (i == 0) {
                    if (selectFeature == null) {
                        weight[i][j] = new double[attrNum + 1];
                    } else {
                        weight[i][j] = new double[selectFeature.length + 1];
                    }
                } else {
                    weight[i][j] = new double[HiddenLayerNodesCount + 1];
                }
            }
        }
        weight[HiddenLayerCount] = new double[1][];
        weight[HiddenLayerCount][0] = new double[HiddenLayerNodesCount + 1];


        double[] oneDimensionalWeight = new double[((attrNum + 1) * HiddenLayerNodesCount) + ((HiddenLayerCount - 1) * ((HiddenLayerNodesCount + 1) * HiddenLayerNodesCount)) + (HiddenLayerNodesCount + 1)];
        int s = 0;
        for (int i = 0; i < chromosome.length - FEATHER_LENGTH; i += WEIGHT_LENGTH) {
            double v = 0;
            for (int j = 1; j < WEIGHT_LENGTH; j++) {
                if (chromosome[i + j])
                    v += Math.pow(2, j - 1);
            }
            if (chromosome[i]) {
                v *= -1;
            }
            oneDimensionalWeight[s++] = v;
        }
        s = 0;
        if (selectFeature == null) {
            for (int j = 0; j < HiddenLayerNodesCount; j++) {
                for (int k = 0; k <= attrNum; k++) {
                    weight[0][j][k] = oneDimensionalWeight[s++] / 127;
                }
            }
        } else {
            for (int j = 0; j < HiddenLayerNodesCount; j++) {
                for (int k = 0; k <= selectFeature.length; k++) {
                    weight[0][j][k] = oneDimensionalWeight[s++] / 127;
                }
            }
        }

        for (int i = 1; i < HiddenLayerCount; i++) {
            for (int j = 0; j < HiddenLayerNodesCount; j++) {
                for (int k = 0; k <= HiddenLayerNodesCount; k++) {
                    weight[i][j][k] = oneDimensionalWeight[s++] / 127;
                }
            }
        }

        for (int k = 0; k <= HiddenLayerNodesCount; k++) {
            weight[HiddenLayerCount][0][k] = oneDimensionalWeight[s++] / 127;
        }

        return weight;
    }

    public Integer[] getSelectFeature() {

        if (FEATHER_LENGTH == 0) return null;

        List<Integer> index = new ArrayList<>();
        int base = (((attrNum + 1) * HiddenLayerNodesCount) + ((HiddenLayerCount - 1) * ((HiddenLayerNodesCount + 1) * HiddenLayerNodesCount)) + (HiddenLayerNodesCount + 1)) * WEIGHT_LENGTH;
        for (int i = base; i < chromosome.length; i++) {
            if (chromosome[i])
                index.add(i - base);
        }

        if (index.size() == 0) {
            Random random = new Random();
            int ranIndex = random.nextInt(FEATHER_LENGTH);
            chromosome[ranIndex] = true;
            index.add(ranIndex);
        }

        return index.toArray(new Integer[index.size()]);
    }

    public Chromosome getMutation() {
        //每組weight, selectFeature(如果有) 各隨機取一個bit做 1->0 & 0->1
        Chromosome newChromosome = new Chromosome(attrNum, HiddenLayerCount, HiddenLayerNodesCount, FEATHER_LENGTH != 0);
        boolean[] newchrom = Arrays.copyOf(chromosome, ChromosomeLength);

        Random random = new Random();
        int FirstLayerWeight = random.nextInt(attrNum);
        newchrom[FirstLayerWeight] = !newchrom[FirstLayerWeight];
        for (int i = 0; i < HiddenLayerCount; i++) {
            int OtherLayerWeight = attrNum + i * HiddenLayerNodesCount + random.nextInt(HiddenLayerNodesCount);
            newchrom[OtherLayerWeight] = !newchrom[OtherLayerWeight];
        }

        newChromosome.setChromosome(newchrom);
        return newChromosome;
    }

    public Chromosome getMutation(double weightMutationRate, double featureMutationRate) {
        //根據突變率，隨機找 長度*突變率 個基因進行突變

        Chromosome newChromosome = new Chromosome(attrNum, HiddenLayerCount, HiddenLayerNodesCount, FEATHER_LENGTH != 0); //建構一個新的chromosome
        boolean[] newchrom = Arrays.copyOf(chromosome, ChromosomeLength); //複製目前chromosome的染色體

        Random random = new Random();
        int WeightMutationCount = (int) ((newchrom.length - FEATHER_LENGTH) * weightMutationRate); //計算需權重部分突變的基因個數
        boolean[] isUsed = new boolean[newchrom.length];

        for (int i = 0; i < WeightMutationCount; i++) {

            int index;
            do {
                index = random.nextInt(newchrom.length - FEATHER_LENGTH);
            } while (isUsed[index]); //找到未被挑過的index

            newchrom[index] = !newchrom[index]; //對該位置進行突變
            isUsed[index] = true;
        }

        if (FEATHER_LENGTH > 0) {

            int FeatureMutationCount = (int) (FEATHER_LENGTH * weightMutationRate); //計算需Feature Selection部分突變的基因個數
            for (int i = 0; i < FeatureMutationCount; i++) {
                int index;
                do {
                    index = random.nextInt(FEATHER_LENGTH) + (newchrom.length - FEATHER_LENGTH);
                } while (isUsed[index]); //找到未被挑過的index

                newchrom[index] = !newchrom[index]; //對該位置進行突變
                isUsed[index] = true;
            }
        }

        newChromosome.setChromosome(newchrom); //將突變後的染色體設到新建構的chromosome中
        return newChromosome;
    }

    public Chromosome getCrossover(Chromosome Chromosome2) {
        return new Chromosome(this, Chromosome2, 0.5);
    }

    public Chromosome getCrossover(Chromosome Chromosome2, double crossRate) {
        return new Chromosome(this, Chromosome2, crossRate);
    }
}
