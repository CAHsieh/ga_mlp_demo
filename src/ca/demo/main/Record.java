package ca.demo.main;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by CANCHU on 2016/11/6.
 */
public class Record {
    private int attrLength;
    private double[] Attributes;
    private int targetClass;

    private String[] originData;
    private ArrayList<Integer> classAttrIndex;

    public Record(String[] originData) {
        this.originData = originData;
        attrLength = originData.length - 1;
        Attributes = new double[attrLength];
        classAttrIndex = new ArrayList<>();
        for (int i = 1; i < originData.length; i++) {
            try {
                Attributes[i - 1] = Double.valueOf(originData[i]);
            } catch (NumberFormatException e) {
                classAttrIndex.add(i);
            }
        }

        try {
            targetClass = Integer.valueOf(originData[0]);
        } catch (NumberFormatException e) {
            targetClass = -1;
        }
    }

    public void ClassAttrToInt(Map<Integer, Map<String, Integer>> map) {
        for (Integer Index : classAttrIndex) {
            Map<String, Integer> mmap = map.get(Index);
            Attributes[Index - 1] = mmap.get(originData[Index]);
        }
    }

    public void setTargetClass(Map<String, Integer> map) {
        targetClass = map.get(originData[0]);
    }

    public ArrayList<Integer> getClassAttrIndex() {
        return classAttrIndex;
    }

    public int getTarget() {
        return targetClass;
    }

    public double getSelectFeature(int i) {
        return Attributes[i];
    }

    public String getOriginDataElement(int i) {
        return originData[i];
    }

    public int getAttrLength() {
        return attrLength;
    }

    public boolean isPredict(int v) {
        return v == targetClass;
    }

    public double getAttributes(int idx) {
        return Attributes[idx];
    }

    public double[] getAllAttributes() {
        return Attributes;
    }

    public double[] getSelectAttributes(Integer[] selectIndex) {
        double[] attr = new double[selectIndex.length];
        for (int i = 0; i < selectIndex.length; i++) {
            attr[i] = Attributes[selectIndex[i]];
        }
        return attr;
    }

    public void setAttributes(int idx, double attributes) {
        Attributes[idx] = attributes;
    }
}
