package ca.demo.main;

import ca.demo.GA.BinaryGA;
import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by CANCHU on 2016/11/5.
 */
public class Main {

    public static void main(String[] args) {

        ArrayList<Record> recordList = new ArrayList<>();

        try {
            File file = new File("defaultdata\\kr-vs-kp.csv");
            String path = file.getAbsolutePath();
            CSVReader reader = new CSVReader(new FileReader(path));
            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                recordList.add(new Record(nextLine));
            }
            reader.close();

            //判斷target及attribute是否有非數字項目
            boolean isTargetClass = recordList.get(0).getTarget() == -1;
            List<Integer> classAttrIndex = recordList.get(0).getClassAttrIndex();
            if (isTargetClass || (classAttrIndex != null && classAttrIndex.size() > 0)) {
                Map<Integer, Map<String, Integer>> map = new HashMap<>();
                Map<String, Integer> targetMap = new HashMap<>();

                int[] attrIdx = new int[classAttrIndex.size()];
                int targetIdx = 0;

                for (Record record : recordList) {

                    if (classAttrIndex.size() > 0) {
                        for (int i = 0; i < classAttrIndex.size(); i++) {
                            Integer index = classAttrIndex.get(i);
                            Map<String, Integer> mmap = map.get(index);
                            if (mmap == null) {
                                mmap = new HashMap<>();
                            }
                            if (!mmap.containsKey(record.getOriginDataElement(index))) {
                                mmap.put(record.getOriginDataElement(index), attrIdx[i]++);
                            }
                            map.put(index, mmap);
                        }
                    }

                    if (isTargetClass) {
                        if (!targetMap.containsKey(record.getOriginDataElement(0))) {
                            targetMap.put(record.getOriginDataElement(0), targetIdx++);
                        }
                    }
                }

                for (Record record : recordList) {
                    record.ClassAttrToInt(map);
                    record.setTargetClass(targetMap);
                }
            }

//            CSVWriter writer = new CSVWriter(new FileWriter(new File("defaultdata\\kr-vs-kp_trans.csv")));
//            for (Record record : recordList) {
//                String[] out = new String[record.getAttrLength() + 1];
//                int i = 0;
//                for (double d : record.getAllAttributes()) {
//                    out[i++] = String.valueOf(d);
//                }
//                out[i] = String.valueOf(record.getTarget());
//                writer.writeNext(out);
//            }

            final int nodesCount = 6;

            File dir = new File("MyOutput");
            if (!dir.exists()) {
                if (!dir.mkdir()) {
                    System.out.println("資料創建失敗。");
                    return;
                }
            }

            new Thread(() -> {
                double goodEnough = 0.97;
                double lastAccuracy;
                int i = 1;
                do {
                    BinaryGA binaryGA = new BinaryGA(recordList, 1, nodesCount, true);
                    System.out.println("\nFEATURE_SELECTION_1_" + nodesCount + "_" + i + " Start!!");
                    lastAccuracy = binaryGA.run("FEATURE_SELECTION_1_" + nodesCount + "_" + i, goodEnough);
                    System.out.println("FEATURE_SELECTION_1_" + nodesCount + "_" + i + " done. Accuracy = " + lastAccuracy);
                    File oldFile = new File(dir.getAbsolutePath() + "\\FEATURE_SELECTION_1_" + nodesCount + "_" + i + "_iteration.csv");
                    File newFile = new File(dir.getAbsolutePath() + "\\FEATURE_SELECTION_1_" + nodesCount + "_" + i + "_iteration_" + lastAccuracy + ".csv");
                    if (!oldFile.renameTo(newFile)) {
                        System.out.println("檔名更改失敗");
                    }
                    i++;
                } while (lastAccuracy < goodEnough && i < 201);
            }).start();

//            int i = 1;
//            double goodEnough = 0.96;
//            double lastAccuracy;
//            do {
//                BinaryGA binaryGA = new BinaryGA(recordList, 1, nodesCount, false);
//                System.out.println("\nNO_FEATURE_SELECTION_1_" + nodesCount + "_" + i + " Start!!");
//                lastAccuracy = binaryGA.run("NO_FEATURE_SELECTION_1_" + nodesCount + "_" + i, goodEnough);
//                System.out.println("NO_FEATURE_SELECTION_1_" + nodesCount + "_" + i + " done. Accuracy = " + lastAccuracy);
//                File oldFile = new File(dir.getAbsolutePath() + "\\NO_FEATURE_SELECTION_1_" + nodesCount + "_" + i + "_iteration.csv");
//                File newFile = new File(dir.getAbsolutePath() + "\\NO_FEATURE_SELECTION_1_" + nodesCount + "_" + i + "_iteration_" + lastAccuracy + ".csv");
//                if (!oldFile.renameTo(newFile)) {
//                    System.out.println("檔名更改失敗");
//                }
//                i++;
//            } while (lastAccuracy < goodEnough && i < 201);

        } catch (IOException ex) {
            ex.printStackTrace();
        }

    }
}
