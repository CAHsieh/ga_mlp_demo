package ca.demo.mlp;

import ca.demo.main.Record;

import java.io.IOException;
import java.util.List;
import java.util.Random;

/**
 * Created by CANCHU on 2016/11/5.
 * 本MLP輸出層僅為一個node
 */
public class MLP {


    private int AttrNum;
    private int HiddenLayerCount;
    private int HiddenLayerNodesCount;

    private Node[][] hiddenLayerNodes;
    private Node outputLayerNode;

    public MLP(int AttrNum, int HiddenLayerCount, int HiddenLayerNodesCount) {
        this.AttrNum = AttrNum;
        this.HiddenLayerCount = HiddenLayerCount;
        this.HiddenLayerNodesCount = HiddenLayerNodesCount;

        hiddenLayerNodes = new Node[HiddenLayerCount][HiddenLayerNodesCount];
    }

    public double[][][] randomW() {
        Random random = new Random();
        double[][][] weight = new double[HiddenLayerCount + 1][HiddenLayerNodesCount][];
        weight[0] = new double[HiddenLayerNodesCount][];
        for (int i = 0; i < HiddenLayerNodesCount; i++) {
            weight[0][i] = new double[AttrNum + 1];
            for (int j = 0; j <= AttrNum; j++) {
                int o = random.nextInt(10001);
                weight[0][i][j] = (double) o / 10000;
                if (random.nextBoolean()) {
                    weight[0][i][j] *= -1;
                }
            }
        }

        for (int i = 1; i < HiddenLayerCount; i++) {
            weight[i] = new double[HiddenLayerNodesCount][];
            for (int j = 0; j < HiddenLayerNodesCount; j++) {
                weight[i][j] = new double[HiddenLayerNodesCount + 1];
                for (int k = 0; k <= HiddenLayerNodesCount; k++) {
                    int o = random.nextInt(10001);
                    weight[i][j][k] = (double) o / 10000;
                    if (random.nextBoolean()) {
                        weight[i][j][k] *= -1;
                    }
                }
            }
        }

        weight[HiddenLayerCount] = new double[1][HiddenLayerNodesCount + 1];
        for (int i = 0; i <= HiddenLayerNodesCount; i++) {
            int o = random.nextInt(10001);
            weight[HiddenLayerCount][0][i] = (double) o / 10000;
            if (random.nextBoolean()) {
                weight[HiddenLayerCount][0][i] *= -1;
            }
        }
        return weight;
    }

    public boolean initialParameter(double[][][] weight, Integer[] selectedFeature) throws IOException {
        //判斷weight數量是否正確
        if (weight == null || weight.length != HiddenLayerCount + 1) {
            throw new IOException();
        } else {
            if (weight[0].length != HiddenLayerNodesCount) {
                throw new IOException();
            }
            if (selectedFeature == null && weight[0][0].length != AttrNum + 1) {
                throw new IOException();
            } else if (selectedFeature != null && weight[0][0].length != selectedFeature.length + 1) {
                throw new IOException();
            }

            for (int i = 1; i <= HiddenLayerCount; i++) {
                if (weight[i][0].length != HiddenLayerNodesCount + 1) {
                    throw new IOException();
                }
            }
            if (weight[HiddenLayerCount][0].length != HiddenLayerNodesCount + 1) {
                throw new IOException();
            }
        }

        //初始化各Node

        for (int j = 0; j < HiddenLayerNodesCount; j++) {
            if (selectedFeature == null)
                hiddenLayerNodes[0][j] = new Node.Builder().setAttrNum(AttrNum + 1).setW(weight[0][j]).build();
            else
                hiddenLayerNodes[0][j] = new Node.Builder().setAttrNum(selectedFeature.length + 1).setW(weight[0][j]).build();
        }

        for (int i = 1; i < HiddenLayerCount; i++) {
            for (int j = 0; j < HiddenLayerNodesCount; j++) {
                hiddenLayerNodes[i][j] = new Node.Builder().setAttrNum(HiddenLayerNodesCount + 1).setW(weight[i][j]).build();
            }
        }

        //輸出層的weight數量應為前一個隱藏層的node數+1
        outputLayerNode = new Node.Builder().setAttrNum(HiddenLayerNodesCount + 1).setW(weight[HiddenLayerCount][0]).build();
        return true;
    }

    public double classify(List<Record> list, Integer[] selectedFeature, double[] threshold) {
        int correct = 0;
        for (Record record : list) {

            double[] in;

            if (selectedFeature != null) {

                double[] originArray = record.getSelectAttributes(selectedFeature);
                in = new double[originArray.length + 1];
                in[0] = -1;
                System.arraycopy(originArray, 0, in, 1, in.length - 1);
            } else {

                double[] originArray = record.getAllAttributes();
                in = new double[originArray.length + 1];
                in[0] = -1;
                System.arraycopy(originArray, 0, in, 1, in.length - 1);
            }

            for (int i = 0; i < hiddenLayerNodes[0].length; i++) {
                hiddenLayerNodes[0][i].calculateOutput(in);
            }

            for (int i = 1; i < hiddenLayerNodes.length; i++) {
                double[] input = new double[HiddenLayerNodesCount + 1];
                input[0] = -1;
                for (int j = 1; j <= hiddenLayerNodes[i - 1].length; j++) {
                    input[j] = hiddenLayerNodes[i - 1][j - 1].getOutput();
                }
                for (int j = 0; j < hiddenLayerNodes[i].length; j++) {
                    hiddenLayerNodes[i][j].calculateOutput(input);
                }
            }

            double[] input = new double[HiddenLayerNodesCount + 1];
            input[0] = -1;
            for (int j = 0; j < hiddenLayerNodes[hiddenLayerNodes.length - 1].length; j++) {
                input[j + 1] = hiddenLayerNodes[hiddenLayerNodes.length - 1][j].getOutput();
            }

            double output = outputLayerNode.calculateOutput(input);

            int index = threshold.length;
            for (int i = 0; i < threshold.length; i++) {
                if (output <= threshold[i]) {
                    index = i;
                    break;
                }
            }

            if (record.isPredict(index))
                correct++;
        }
        return (double) correct / list.size();
    }
}
