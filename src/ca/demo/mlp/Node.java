package ca.demo.mlp;

/**
 * Created by CANCHU on 2016/11/5.
 */
public class Node {

    private int AttrNum;
    private double[] w;

    private Double output;

    private Node() {
        AttrNum = 1;
        w = new double[AttrNum];
        output = null;
    }

    private void setAttrNum(int attrNum) {
        AttrNum = attrNum;
    }

    private void setW(double[] w) {
        this.w = w;
    }


    public int getAttrNum() {
        return AttrNum;
    }

    public double[] getW() {
        return w;
    }

    public Double calculateOutput(double[] attr) {
        double sum = 0;
        for (int i = 0; i < attr.length; i++) {
            sum += (attr[i] * w[i]);
        }
        sum *= -1;
        output = 1.0 / (1 + Math.exp(sum));
        return output;
    }

    public Double getOutput() {
        return output;
    }

    public static class Builder {
        Node node;

        public Builder() {
            node = new Node();
        }

        public Builder setAttrNum(int AttrNum) {
            node.setAttrNum(AttrNum);
            return this;
        }

        public Builder setW(double[] w) {
            node.setW(w);
            return this;
        }

        public Node build() {
            return node;
        }
    }
}
